package sample;

import javafx.scene.control.Button;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Decisions {

    Question question;
    QuestionList questionList = new QuestionList();
    static List<String> history = new ArrayList<>();
    Integer last_question = 0;
    List<Integer> index_history = new ArrayList<>();

    public Question finish(){
        Question question = new Question("Wynik",new ArrayList<>());
        return question;
    }

    public Question make_decision(String answer){
        history.add(answer);
        System.out.println(history.get(history.size()-1));
        // Laptops ---------------------------------------------------------------------------------------------------//
        if (history.get(0).equals("Laptop")) {
            if(index_history.size()>0){
                last_question = index_history.get(index_history.size()-1); }
            if (history.size() == 1) {
                index_history.add(0); return questionList.laptop_index.get(0);
            }
            // Type //
            else if (history.size() == 2) {
                if (history.get(1) == "Gra") {
                    question = questionList.laptop_index.get(1);
                } else if (history.get(1) == "Praca") {
                    index_history.add(4); return questionList.laptop_index.get(4);
                } else if (history.get(1) == "Przeglądanie internetu") {
                    index_history.add(11); return questionList.laptop_index.get(11);
                }
            }
            // Game //
            else if (history.get(1) == "Gra" && history.size() == 3) {
                if (history.get(2)== "3000 - 5000 zł") {
                    index_history.add(2); return questionList.laptop_index.get(2);
                } else if (history.get(2)== "5000 - 7000 zł") {
                    index_history.add(2); return questionList.laptop_index.get(2);
                } else if (history.get(2)== "7000 - 10000 zł") {
                    index_history.add(2); return questionList.laptop_index.get(2);
                } else if (history.get(2)== "10000 - 20000 zł") {
                    index_history.add(2); return questionList.laptop_index.get(2);
                }
            }
            else if (history.get(1) == "Gra" && history.size() == 4) {
                if (history.get(2)=="3000 - 5000 zł" || history.get(2)=="5000 - 7000 zł"){
                    index_history.add(3); return questionList.laptop_index.get(3);
                } else {
                    index_history.add(14); return questionList.laptop_index.get(14);
                }
            } else if (history.get(1) == "Gra" && history.size() == 5) {
                return finish();
            }
            // Work, Uniwersal //
            else if (history.get(1) == "Praca" && history.size() == 3) {
                if (history.get(2)== "iOS") {
                    index_history.add(5); return questionList.laptop_index.get(5);
                } else if (history.get(2)== "Windows") {
                    index_history.add(8); return questionList.laptop_index.get(8);
                }else {
                    index_history.add(16); return questionList.laptop_index.get(16);
                }
            }
            // iOS Price //
            if (last_question==5) {
                if (history.get(3)== "4000 - 7000 zł" || history.get(3)== "7000 - 10000 zł") {
                    index_history.add(6); return questionList.laptop_index.get(6);
                } else {
                    index_history.add(7); return questionList.laptop_index.get(7);
                }
            }
            // Windows Price //
            else if (last_question==8) {
                if (history.get(3)== "2000 - 4000 zł" || history.get(3)== "4000 - 60000 zł") {
                    index_history.add(6); return questionList.laptop_index.get(6);
                } else if (history.get(3)== "10000 - 20000 zł"){
                    index_history.add(15); return questionList.laptop_index.get(15);
                } else {
                    index_history.add(7); return questionList.laptop_index.get(7);
                }
            }
            // No OS Price //
            else if(last_question==16){
                return finish();
            }
            else if (history.get(1) == "Praca" && history.size() == 4) {
                if (history.get(3)== "2000 - 4000 zł" || history.get(3)== "4000 - 60000 zł") {
                    index_history.add(9); return questionList.laptop_index.get(9);
                } else {
                    index_history.add(10); return questionList.laptop_index.get(10);
                }
            }
            else if (last_question == 6 || last_question == 7 || last_question == 15 || last_question == 16){
                return finish();
            }
            // Internet //
            else if (history.get(1) == "Przeglądanie internetu" && history.size() == 3) {
                if (history.get(2) == "Funkcja Tabletu (2w1)"){
                    index_history.add(13); return questionList.laptop_index.get(13);
                } else if (history.get(2) == "Wytrzymała Bateria"){
                    index_history.add(12); return questionList.laptop_index.get(12);
                }
            }
            if (history.get(1) == "Przeglądanie internetu" && (last_question==12 || last_question==13)){
                return finish();
            }
            return question;
        // Tablets ---------------------------------------------------------------------------------------------------//
        } else if (history.get(0).equals("Tablet")) {
            if(index_history.size()>0){
                 last_question = index_history.get(index_history.size()-1); }
            // OS //
            if (history.size() == 1){
                question = questionList.tablet_index.get(0); index_history.add(0); }
            // OS -> price //
            else if (last_question == 0){
                if (history.get(1).equals("Android")) {
                    question = questionList.tablet_index.get(1); index_history.add(1);
                } else if (history.get(1).equals("iOS")) {
                    question = questionList.tablet_index.get(2); index_history.add(2);
                } else if (history.get(1).equals("Windows")) {
                    question = questionList.tablet_index.get(3); index_history.add(3); }
            }
            // Windows price -> display_size //
            else if (last_question == 3){
                if (history.get(2).equals("800-1500 zł")) {
                    question = questionList.tablet_index.get(12); index_history.add(12);
                } else if (history.get(2).equals("1500-3000 zł")) {
                    question = questionList.tablet_index.get(13); index_history.add(13); }
            }
            // Windows display_size //
            else if (last_question == 12){
                if (history.get(3).equals("10.1")) {return finish();}
                else if (history.get(3).equals("11.6")) {return finish();}
            }
            else if (last_question == 13){
                if (history.get(3).equals("8")) {return finish();}
                else if (history.get(3).equals("10")) {return finish();}
                else if (history.get(3).equals("12")) {return finish();}
            }
            // Android price -> display_size //
            else if (last_question == 1){
                if (history.get(2).equals("200-800 zł")) {
                    question = questionList.tablet_index.get(8);index_history.add(8);
                } else if (history.get(2).equals("800-1500 zł")) {
                    question = questionList.tablet_index.get(9); index_history.add(9);
                } else if (history.get(2).equals("1500-3500 zł")) {
                    question = questionList.tablet_index.get(9); index_history.add(9); }
            }
            // Android display_size -> memory //
            else if (last_question == 8){
                if (history.get(3).equals("7")) {return finish();}
                else if (history.get(3).equals("8")) {return finish();}
                else if (history.get(3).equals("10.1")) {
                    question = questionList.tablet_index.get(10); index_history.add(10); }
            }
            else if (last_question == 9){
                if (history.get(3).equals("8")) {return finish();}
                else if (history.get(2).equals("800-1500 zł") && history.get(3).equals("10")) {
                    question = questionList.tablet_index.get(10); index_history.add(10);
                } else if (history.get(2).equals("1500-3500 zł") && history.get(3).equals("10")) {
                    question = questionList.tablet_index.get(11); index_history.add(11); }
            }
            // Android memory //
            else if (last_question == 10){
                if (history.get(4).equals("16 GB")) {return finish();}
                else if (history.get(4).equals("32 GB")) {return finish();}
            }
            else if (last_question == 11){
                if (history.get(4).equals("32 GB")) {return finish();}
                else if (history.get(4).equals("64 GB")) {return finish();}
            }
            // iOS price -> display_size //
            else if (last_question == 2) {
                if (history.get(2).equals("1500-3000 zł")) {
                    question = questionList.tablet_index.get(4); index_history.add(4);
                } else if (history.get(2).equals("3000-6200 zł")) {
                    question = questionList.tablet_index.get(5); index_history.add(5); }
            }
            // iOS display_size -> memory //
            else if (last_question == 5) {
                question = questionList.tablet_index.get(14); index_history.add(14);
            } else if (last_question == 4) {
                if (history.get(3).equals("9.7")) {
                    question = questionList.tablet_index.get(6); index_history.add(6); }
                else if (history.get(3).equals("7.9")) {return finish();}
                else if (history.get(3).equals("10.5")) {return finish();}
            }
            else if (last_question == 6 || last_question == 7 || last_question == 14) {return finish();}
            return question;
        }
        // Smartphone ------------------------------------------------------------------------------------------------//
        else if (history.get(0).equals("Smartphone")) {
            if(index_history.size()>0){
                last_question = index_history.get(index_history.size()-1); }
            if (history.size() == 1){
                question = questionList.smartphone_index.get(0); index_history.add(0); }
            // Type  //
            else if (last_question == 0){
                if (history.get(1)=="Gra"){
                    question = questionList.smartphone_index.get(1); index_history.add(1);
                } else if (history.get(1)=="Wytrzymałość obudowy"){
                    question = questionList.smartphone_index.get(2); index_history.add(2);
                } else if (history.get(1)=="Najlepszy Aparat") {
                    question = questionList.smartphone_index.get(15); index_history.add(15);
                }
                else if (history.get(1)=="Uniwersalny"){
                    question = questionList.smartphone_index.get(3); index_history.add(3); }
            }
            else if (last_question == 1 || last_question == 2 || last_question == 15){return finish();}
            // OS //
            else if (last_question == 3){
                if (history.get(2)=="Windows"){return finish();}
                else if (history.get(2)=="BlackBerry"){return finish();}
                else if (history.get(2)=="iOS"){
                    question = questionList.smartphone_index.get(4); index_history.add(4); }
                else if (history.get(2)=="Android"){
                    question = questionList.smartphone_index.get(9); index_history.add(9); }
            }
            // iOS price //
            else if (last_question == 4){
                if (history.get(3)=="1000-2500 zł"){
                    question = questionList.smartphone_index.get(5); index_history.add(5); }
                else if (history.get(3)=="2500-4000 zł"){
                    question = questionList.smartphone_index.get(6); index_history.add(6); }
                else if (history.get(3)=="4000-5500 zł"){
                    question = questionList.smartphone_index.get(7); index_history.add(7); }
                else if (history.get(3)=="5500-7500 zł"){
                    question = questionList.smartphone_index.get(8); index_history.add(8); }
            }
            else if (last_question == 5 || last_question == 6 || last_question == 7 ||last_question == 8){
                return finish(); }
            // Android price --> DualSim //
            else if (last_question == 9){
                if (history.get(3)=="0-500 zł"){
                    question = questionList.smartphone_index.get(10); index_history.add(10); }
                else if (history.get(3)=="500-1500 zł"){
                    question = questionList.smartphone_index.get(10); index_history.add(10); }
                else if (history.get(3)=="1500-2500 zł"){
                    question = questionList.smartphone_index.get(10); index_history.add(10); }
                else if (history.get(3)=="2500-4000 zł"){
                    question = questionList.smartphone_index.get(10); index_history.add(10); }
                else if (history.get(3)=="4000-5500 zł"){
                    question = questionList.smartphone_index.get(14); index_history.add(14); }
            }
            // Android DualSim --> Memory //
            else if (last_question == 10){
                if (history.get(3)=="0-500 zł"){
                    question = questionList.smartphone_index.get(11); index_history.add(11); }
                else if (history.get(3)=="500-1500 zł"){
                    question = questionList.smartphone_index.get(12); index_history.add(12); }
                else if (history.get(3)=="1500-2500 zł"){
                    question = questionList.smartphone_index.get(13); index_history.add(13); }
                else if (history.get(3)=="2500-4000 zł"){
                    question = questionList.smartphone_index.get(13); index_history.add(13); }
            }
            else if (last_question == 11 || last_question == 12 || last_question == 13 || last_question == 14) {
                return finish(); }
            return question;
        }
        // Error -----------------------------------------------------------------------------------------------------//
        else {
            List<String> a = new ArrayList<>();
            a.add("error");
            return new Question("error", a);
        }
    }
}
