package sample;

public class Laptop {
    public String name;
    public Double price;
    public String company;
    public String OS;
    public String OS_version;
    public Double display_size;
    public String graphic_card;
    public String graphic_card_prod = "";
    public Integer disk;
    public Integer RAM;
    public String type;
    public String processor;
    public Double battery;
    public Double weight;
    public String additional;
}
