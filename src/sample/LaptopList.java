package sample;


import java.util.ArrayList;
import java.util.List;

public class LaptopList {

    List<Laptop> laptop_list = new ArrayList<>();
    Laptop new_laptop ;

    LaptopList(){

        // Internet -> Battery //
        new_laptop = new Laptop();
        new_laptop.name = "ThinkPad T470";
        new_laptop.company = "Lenovo";
        new_laptop.price = 6979.00;
        new_laptop.display_size = 14.1;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "Intel HD Graphics 620";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i5";
        new_laptop.type = "Battery";
        new_laptop.battery = 4500.00;
        new_laptop.additional = "Wytrzymałość baterii do 13h";
        new_laptop.weight = 2.15;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "MacBook Air";
        new_laptop.company = "Apple";
        new_laptop.price = 3999.00;
        new_laptop.display_size = 13.4;
        new_laptop.disk = 128;
        new_laptop.graphic_card = "Intel HD Graphics 6000";
        new_laptop.OS = "iOS";
        new_laptop.OS_version = "Sierra";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i5";
        new_laptop.type = "Battery";
        new_laptop.battery = 5000.00;
        new_laptop.additional = "Wytrzymałość baterii do ciągłej pracy 13h";
        new_laptop.weight = 1.35;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "XPS 13 9360 ";
        new_laptop.company = "Dell";
        new_laptop.price = 5899.00;
        new_laptop.display_size = 13.3;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "Intel UHD Graphics 620";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i7-8250U";
        new_laptop.type = "Battery";
        new_laptop.battery = 8085.00;
        new_laptop.additional = "Wytrzymałość baterii do 20h";
        new_laptop.weight = 1.65;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "ThinkPad T460p ";
        new_laptop.company = "Lenovo";
        new_laptop.price = 8850.00;
        new_laptop.display_size = 14.1;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "NVIDIA GeForce 940MX / Intel HD Graphics 530";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i7";
        new_laptop.type = "Battery";
        new_laptop.battery = 7025.00;
        new_laptop.additional = "Wytrzymałość baterii do 15h";
        new_laptop.weight = 1.45;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "gram Thin ";
        new_laptop.company = "LG";
        new_laptop.price = 3560.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "Nividia GeForce 940MX / Intel HD Graphics 530";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i5-8250U";
        new_laptop.type = "Battery";
        new_laptop.battery = 5025.00;
        new_laptop.weight = 2.53;
        new_laptop.additional = "Wytrzymałość baterii do 12h";
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = " YOGA 920 ";
        new_laptop.company = "Lenovo";
        new_laptop.price = 8100.00;
        new_laptop.display_size = 13.9;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "Intel UHD Graphics 620";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i5-8250U";
        new_laptop.type = "Battery";
        new_laptop.battery = 7400.00;
        new_laptop.additional = "Wytrzymałość baterii do 15h";
        new_laptop.weight = 1.35;
        laptop_list.add(new_laptop);

        // Internet -> 2 in 1 //
        new_laptop = new Laptop();
        new_laptop.name = "YOGA 310-11 N3350";
        new_laptop.company = "Lenovo";
        new_laptop.price = 1250.00;
        new_laptop.display_size = 11.6;
        new_laptop.disk = 64;
        new_laptop.graphic_card = "Intel HD Graphics 500";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 2;
        new_laptop.processor = "Intel Core i5-8250U";
        new_laptop.type = "2in1";
        new_laptop.battery = 2800.00;
        new_laptop.additional = "Dotykowy Ekran, Mały przenośny";
        new_laptop.weight = 1.35;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "VivoBook Flip ";
        new_laptop.company = "ASUS";
        new_laptop.price = 3350.00;
        new_laptop.display_size = 14.1;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "Intel UHD Graphics 620";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i5-8250U";
        new_laptop.type = "2in1";
        new_laptop.battery = 3600.00;
        new_laptop.additional = "Ekran 360 st., Czytnik linii papilarnych";
        new_laptop.weight = 1.55;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "Lenovo YOGA 720";
        new_laptop.company = "Lenovo";
        new_laptop.price = 4560.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "Intel HD Graphics 500";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i5";
        new_laptop.type = "2in1";
        new_laptop.battery = 3600.00;
        new_laptop.additional = "Czytnik lini papilarnych, Ekran dotykowy";
        new_laptop.weight = 1.70;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "Alcatel Plus 10";
        new_laptop.company = "Alcatel";
        new_laptop.price = 550.00;
        new_laptop.display_size = 10.1;
        new_laptop.disk = 32;
        new_laptop.graphic_card = "Intel HD Graphics";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 2;
        new_laptop.processor = "Intel Core i5-8250U";
        new_laptop.type = "2in1";
        new_laptop.battery = 2500.00;
        new_laptop.additional = "Mały przenośny";
        new_laptop.weight = 0.80;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "Surface Book 2";
        new_laptop.company = "Microsoft";
        new_laptop.price = 7500.00;
        new_laptop.display_size = 13.5;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "Intel HD Graphics 620";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i5-7300U";
        new_laptop.type = "2in1";
        new_laptop.battery = 4200.00;
        new_laptop.additional = "Mały przenośny";
        new_laptop.weight = 1.50;
        laptop_list.add(new_laptop);

        // Work -> iOS //
        new_laptop = new Laptop();
        new_laptop.name = "MacBook Pro";
        new_laptop.company = "Apple";
        new_laptop.price = 5600.00;
        new_laptop.display_size = 13.3;
        new_laptop.disk = 128;
        new_laptop.graphic_card = "Intel Iris Graphics 640";
        new_laptop.OS = "iOS";
        new_laptop.OS_version = "Sierra";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i5-7300U";
        new_laptop.type = "Work";
        new_laptop.battery = 4100.00;
        new_laptop.additional = "Wszechstronny port Thunderbolt 3, Ekran Retina";
        new_laptop.weight = 1.38;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "MacBook Pro";
        new_laptop.company = "Apple";
        new_laptop.price = 6200.00;
        new_laptop.display_size = 13.3;
        new_laptop.disk = 128;
        new_laptop.graphic_card = "Intel Iris Graphics 640";
        new_laptop.OS = "iOS";
        new_laptop.OS_version = "Sierra";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i5-7300U";
        new_laptop.type = "Work";
        new_laptop.battery = 4200.00;
        new_laptop.additional = "Wszechstronny port Thunderbolt 3, Ekran Retina";
        new_laptop.weight = 1.37;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "MacBook Pro";
        new_laptop.company = "Apple";
        new_laptop.price = 7500.00;
        new_laptop.display_size = 15.1;
        new_laptop.disk = 128;
        new_laptop.graphic_card = "Intel Iris Graphics 640";
        new_laptop.OS = "iOS";
        new_laptop.OS_version = "Sierra";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i5-7300U";
        new_laptop.type = "Work";
        new_laptop.battery = 4500.00;
        new_laptop.additional = "Wszechstronny port Thunderbolt 3, Ekran Retina";
        new_laptop.weight = 1.58;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "MacBook Pro";
        new_laptop.company = "Apple";
        new_laptop.price = 8900.00;
        new_laptop.display_size = 15.4;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "Intel Iris Plus Graphics 655";
        new_laptop.OS = "iOS";
        new_laptop.OS_version = "macOS High Sierra";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i5 8 generacji";
        new_laptop.type = "Work";
        new_laptop.battery = 4500.00;
        new_laptop.additional = "Wszechstronny port Thunderbolt 3, Ekran Retina";
        new_laptop.weight = 1.37;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "MacBook Pro";
        new_laptop.company = "Apple";
        new_laptop.price = 11500.00;
        new_laptop.display_size = 15.4;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "Radeon Pro 555X";
        new_laptop.OS = "iOS";
        new_laptop.OS_version = "macOS High Sierra";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i7 8 gen.";
        new_laptop.type = "Work";
        new_laptop.battery = 5500.00;
        new_laptop.additional = "TouchBar, Ekran Retina";
        new_laptop.weight = 1.83;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "MacBook Pro";
        new_laptop.company = "Apple";
        new_laptop.price = 14299.00;
        new_laptop.display_size = 15.4;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "Radeon Pro 555X";
        new_laptop.OS = "iOS";
        new_laptop.OS_version = "macOS High Sierra";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Core i7 8 gen.";
        new_laptop.type = "Work";
        new_laptop.battery = 5300.00;
        new_laptop.additional = "TouchBar, Ekran UHD";
        new_laptop.weight = 1.37;
        laptop_list.add(new_laptop);


        new_laptop = new Laptop();
        new_laptop.name = "MacBook Pro";
        new_laptop.company = "Apple";
        new_laptop.price = 24500.00;
        new_laptop.display_size = 15.4;
        new_laptop.disk = 2048;
        new_laptop.graphic_card = "Radeon 560X Silver";
        new_laptop.OS = "iOS";
        new_laptop.OS_version = "macOS High Sierra";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Core i9 8. gen";
        new_laptop.type = "Work";
        new_laptop.battery = 4900.00;
        new_laptop.additional = "TouchBar, Ekran Retina";
        new_laptop.weight = 1.83;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "MacBook Pro";
        new_laptop.company = "Apple";
        new_laptop.price = 18399.00;
        new_laptop.display_size = 13.3;
        new_laptop.disk = 2048;
        new_laptop.graphic_card = "Intel Iris Plus Graphics 655";
        new_laptop.OS = "iOS";
        new_laptop.OS_version = "macOS High Sierra";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i7 8. gen.";
        new_laptop.type = "Work";
        new_laptop.battery = 5200.00;
        new_laptop.additional = "TouchBar, Ekran Retina";
        new_laptop.weight = 1.37;
        laptop_list.add(new_laptop);

        // Work Windows //
        new_laptop = new Laptop();
        new_laptop.name =  "Ideapad 120";
        new_laptop.company = "Lenovo";
        new_laptop.price = 3199.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 128;
        new_laptop.graphic_card = "Intel HD Graphics 505 + AMD Radeon 530";
        new_laptop.graphic_card_prod = "AMD";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4300.00;
        new_laptop.additional = "Port typu C, Krystalicznie czysty obraz Full HD";
        new_laptop.weight = 1.44;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "Ideapad 120";
        new_laptop.company = "Lenovo";
        new_laptop.price = 2900.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 128;
        new_laptop.graphic_card = "Intel HD Graphics 505 + AMD Radeon 535";
        new_laptop.graphic_card_prod = "AMD";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4300.00;
        new_laptop.additional = "Port typu C, Krystalicznie czysty obraz Full HD";
        new_laptop.weight = 1.44;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "Ideapad 120";
        new_laptop.company = "Lenovo";
        new_laptop.price = 3599.00;
        new_laptop.display_size = 14.0;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "Intel HD Graphics 505 + AMD Radeon 535";
        new_laptop.graphic_card_prod = "AMD";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4300.00;
        new_laptop.additional = "Port typu C, Krystalicznie czysty obraz Full HD";
        new_laptop.weight = 1.44;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "GV62";
        new_laptop.company = "MSI";
        new_laptop.price = 4299.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "Nividia GeForce GTX 1050+Intel UHD Graphics 630";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i7-8750H";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4400.00;
        new_laptop.additional = "Port typu C, Krystalicznie czysty obraz Full HD";
        new_laptop.weight = 2.1;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "GV62";
        new_laptop.company = "MSI";
        new_laptop.price = 5299.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "Nividia GeForce GTX 1050+Intel UHD Graphics 630";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i7-8750H";
        new_laptop.type = "Work";
        new_laptop.battery = 4400.00;
        new_laptop.additional = "Port typu C, Krystalicznie czysty obraz Full HD";
        new_laptop.weight = 2.1;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "ASUS Vivobook Pro";
        new_laptop.company = "Lenovo";
        new_laptop.price = 5699.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "GeForce GTX 1050+Intel UHD Graphics 630";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work";
        new_laptop.battery = 4200.00;
        new_laptop.additional = "Port typu C, Krystalicznie czysty obraz Full HD";
        new_laptop.weight = 1.9;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "ASUS Vivobook Pro";
        new_laptop.company = "Lenovo";
        new_laptop.price = 5199.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 128;
        new_laptop.graphic_card = "GeForce GTX 1050+Intel UHD Graphics 630";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4200.00;
        new_laptop.additional = "Port typu C, Rozdzielczość Full HD";
        new_laptop.weight = 1.9;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "ASUS Vivobook Pro";
        new_laptop.company = "Lenovo";
        new_laptop.price = 6799.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "GeForce GTX 1050+Intel UHD Graphics 630";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work";
        new_laptop.battery = 4200.00;
        new_laptop.additional = "Port typu C, Krystalicznie czysty obraz Full HD";
        new_laptop.weight = 1.9;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "Inspiron G5";
        new_laptop.company = "Dell";
        new_laptop.price = 7299.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "GeForce GTX 1060 MaxQ";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4200.00;
        new_laptop.additional = "Port typu C, Ekran 360st.";
        new_laptop.weight = 2.85;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "Inspiron 5570";
        new_laptop.company = "Dell";
        new_laptop.price = 5599.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "Radeon 530";
        new_laptop.graphic_card_prod = "AMD";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4200.00;
        new_laptop.additional = "Port typu C, HDMI";
        new_laptop.weight = 2.85;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "Inspiron 5570";
        new_laptop.company = "Dell";
        new_laptop.price = 5100.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "Radeon 530";
        new_laptop.graphic_card_prod = "AMD";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4200.00;
        new_laptop.additional = "Port typu C, HDMI";
        new_laptop.weight = 2.85;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "GS65";
        new_laptop.company = "MSI";
        new_laptop.price = 9599.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "GeForce GTX 1070 Max-Q";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i7-8750H";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 5300.00;
        new_laptop.additional = "Dragon Center 2.0, 144Hz ekran z wąską ramką i czasem reakcji 7ms";
        new_laptop.weight = 1.8;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "OMEN 17";
        new_laptop.company = "HP";
        new_laptop.price = 9100.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 256;
        new_laptop.graphic_card = "GTX1070";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Core i7-8750H";
        new_laptop.type = "Game";
        new_laptop.battery = 5900.00;
        new_laptop.additional = "Obsługa systemu Oculus, Ekran IPS 120Hz";
        new_laptop.weight = 3.3;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "Dell XPS 15";
        new_laptop.company = "Dell";
        new_laptop.price = 15599.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 2048;
        new_laptop.graphic_card = "GeForce GTX 1050Ti";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Core i9-8950HK";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 5400.00;
        new_laptop.additional = "Ekran 4k";
        new_laptop.weight = 2.07;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "Inspiron G5";
        new_laptop.company = "Dell";
        new_laptop.price = 8500.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "GeForce GTX 1050Ti";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Core i9-8950HK";
        new_laptop.type = "Work";
        new_laptop.battery = 5200.00;
        new_laptop.additional = "Ekran Full HD, Podświtlana klawiatura";
        new_laptop.weight = 2.07;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "GS65";
        new_laptop.company = "MSI";
        new_laptop.price = 12500.00;
        new_laptop.display_size = 15.6;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "Nividia GeForce GTX 1070 Max-Q";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Core i7-8750H";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 5300.00;
        new_laptop.additional = "Dragon Center 2.0, 144Hz ekran z wąską ramką i czasem reakcji 7ms";
        new_laptop.weight = 1.8;
        laptop_list.add(new_laptop);

        //Bez systemu op.//
        new_laptop = new Laptop();
        new_laptop.name = "GS1070";
        new_laptop.company = "Dream Machines";
        new_laptop.price = 9500.00;
        new_laptop.display_size = 17.3;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "GeForce GTX 1070 ";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Bez systemu op.";
        new_laptop.OS_version = "brak";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Core i9";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 5300.00;
        new_laptop.additional = "Matowa matryca, Ostre i wyraźne kolory na matrycy IPS";
        new_laptop.weight = 2.4;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "VivoBook";
        new_laptop.company = "ASUS ";
        new_laptop.price = 2400.00;
        new_laptop.display_size = 17.3;
        new_laptop.disk = 128;
        new_laptop.graphic_card = "GeForce GTX 1070";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Bez systemu op.";
        new_laptop.OS_version = "brak";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i3-8130";
        new_laptop.type = "Work";
        new_laptop.battery = 3600.00;
        new_laptop.additional = "ASUS IceCool dba o niską temperaturę pracy";
        new_laptop.weight = 2.1;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "VivoBook";
        new_laptop.company = "ASUS ";
        new_laptop.price = 3500.00;
        new_laptop.display_size = 17.3;
        new_laptop.disk = 128;
        new_laptop.graphic_card = "GeForce GTX 1070";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "8";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i3-8130";
        new_laptop.type = "Game";
        new_laptop.battery = 3600.00;
        new_laptop.additional = "Dobre chłodzenie";
        new_laptop.weight = 2.1;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "TUF";
        new_laptop.company = "ASUS";
        new_laptop.price = 4400.00;
        new_laptop.display_size = 17.3;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "GeForce GTX 1050";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Bez systemu op.";
        new_laptop.OS_version = "brak";
        new_laptop.RAM = 8;
        new_laptop.processor = "Intel Core i5-8380";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4260.00;
        new_laptop.additional = "ASUS IceCool dba o niską temperaturę pracy";
        new_laptop.weight = 2.5;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "ROG Strix SCAR";
        new_laptop.company = "ASUS";
        new_laptop.price = 6550.00;
        new_laptop.display_size = 17.3;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "GeForce GTX 1050";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Bez systemu op.";
        new_laptop.OS_version = "brak";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i7";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4960.00;
        new_laptop.additional = "Maksymalna przepustowość łącza, Lepsze wrażenia dźwiękowe";
        new_laptop.weight = 2.2;
        laptop_list.add(new_laptop);

        // Game //
        new_laptop = new Laptop();
        new_laptop.name = "XPS 15";
        new_laptop.company = "Dell";
        new_laptop.price = 11550.00;
        new_laptop.display_size = 17.3;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "AMD Radeon RX Vega M GL,";
        new_laptop.graphic_card_prod = "AMD";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i7";
        new_laptop.type = "Game";
        new_laptop.battery = 4960.00;
        new_laptop.additional = "Ekran UHD, 360 st.";
        new_laptop.weight = 2.3;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "XPS 15";
        new_laptop.company = "Dell";
        new_laptop.price = 15950.00;
        new_laptop.display_size = 17.3;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "AMD Radeon RX Vega M GL,";
        new_laptop.graphic_card_prod = "AMD";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Core i7";
        new_laptop.type = "Game";
        new_laptop.battery = 5960.00;
        new_laptop.additional = "Ekran UHD, 360 st.";
        new_laptop.weight = 2.3;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name = "Helios 500";
        new_laptop.company = "Acer";
        new_laptop.price = 12050.00;
        new_laptop.display_size = 17.3;
        new_laptop.disk = 480;
        new_laptop.graphic_card = "GeForce GTX 1070";
        new_laptop.graphic_card_prod = "Nividia";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Core i9-8950";
        new_laptop.type = "Game";
        new_laptop.battery = 4860.00;
        new_laptop.additional = "Ekran UHD, 360 st.";
        new_laptop.weight =3.5;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "Helios";
        new_laptop.company = "Acer";
        new_laptop.price = 7199.00;
        new_laptop.display_size = 17.3;
        new_laptop.disk = 512;
        new_laptop.graphic_card = "Intel HD Graphics 505 + AMD Radeon 530";
        new_laptop.graphic_card_prod = "AMD";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 16;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4300.00;
        new_laptop.additional = "Port typu C, Krystalicznie czysty obraz Full HD";
        new_laptop.weight = 3.44;
        laptop_list.add(new_laptop);

        new_laptop = new Laptop();
        new_laptop.name =  "Helios";
        new_laptop.company = "Acer";
        new_laptop.price = 9299.00;
        new_laptop.display_size = 17.3;
        new_laptop.disk = 512;
        new_laptop.graphic_card = " AMD RX 550";
        new_laptop.graphic_card_prod = "AMD";
        new_laptop.OS = "Windows";
        new_laptop.OS_version = "10 Home";
        new_laptop.RAM = 32;
        new_laptop.processor = "Intel Pentium N4200";
        new_laptop.type = "Work,Game";
        new_laptop.battery = 4300.00;
        new_laptop.additional = "Port typu C, Krystalicznie czysty obraz Full HD";
        new_laptop.weight = 4.00;
        laptop_list.add(new_laptop);
    }



}
