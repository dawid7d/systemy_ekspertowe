package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.scene.control.*;

import java.util.List;

public class Main extends Application {

    static Decisions decisions = new Decisions();
    QuestionList questionList = new QuestionList();
    Question question;
    VBox vBox = new VBox(20);
    HBox hBox = new HBox(0);
    StackPane stackPane_question = new StackPane();
    private Integer width = 1000;
    private Integer hight = 700;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        question = questionList.question_index.get(0);
        set_question(question);

        Scene scene = new Scene(vBox, width, hight);
        primaryStage.setTitle("System ekspertowy wybór odpowiedniego urządzenia mobilnego");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void trigger_decision(Button button) {
        Question new_question;
        new_question = decisions.make_decision(button.getText());
            if(!new_question.question.equals("Wynik")){
                set_question(new_question);
            }
            else if (Decisions.history.get(0)=="Tablet") {
                set_tablet_result(Result.make_tablet_result(Decisions.history));
            }
            else if (Decisions.history.get(0)=="Smartphone") {
                set_smartphone_result(Result.make_smartphone_result(Decisions.history));
            }
            else if (Decisions.history.get(0)=="Laptop") {
                set_laptop_result(Result.make_laptop_result(Decisions.history));
            }
    }

    public void set_question(Question question){
        // Clear //
        stackPane_question.getChildren().clear();
        vBox.getChildren().clear();
        // Add question //
        Text question_text = new Text(question.question);
        question_text.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        stackPane_question.getChildren().add(question_text);
        stackPane_question.setPadding(new Insets(20, 20, 20, 20));
        stackPane_question.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        vBox.getChildren().add(stackPane_question);
        vBox.getChildren().add(new StackPane(new Rectangle(1,70,Color.TRANSPARENT)));
        // Add answers //
        for (String answer : question.answers) {
            Button button = new Button(answer);
            button.setMinWidth(200);
            button.setMinHeight(40);
            button.setOnAction(event -> trigger_decision((Button) event.getSource()));
            button.setFont(Font.font("Verdana", 15));
            vBox.getChildren().add(button);
        }
        // Set VBox //
        vBox.setSpacing(40);
        vBox.setAlignment(Pos.CENTER);
    }

    // Tablet Result //
    public void set_tablet_result(List<Tablet> tabletList) {
        stackPane_question.getChildren().clear();
        vBox.getChildren().clear();
        // History //
        Text history;
        history = new Text("Historia:   \n");
        StackPane stackPane_history = new StackPane();
        history.setFont(Font.font("Verdana", FontWeight.BOLD, 20));;
        stackPane_history.getChildren().add(history);
        stackPane_history.setPadding(new Insets(20, 20, 20, 20));
        stackPane_history.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        vBox.getChildren().add(stackPane_history);
        for(String h : Decisions.history) {
            if (Decisions.history.indexOf(h) == Decisions.history.size() - 1) {
                history = new Text(h);
            } else {
                history = new Text(h + "   --->  ");
            }
            history.setFont(Font.font("Verdana", FontWeight.MEDIUM, 15));;
            hBox.getChildren().add(history);
        }
        hBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(hBox);
        // End //
        Text question_text = new Text("Wynik:");
        question_text.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        stackPane_question.getChildren().add(question_text);
        stackPane_question.setPadding(new Insets(20, 20, 20, 20));
        stackPane_question.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        vBox.getChildren().add(stackPane_question);

        // Results //
        ScrollPane sp = new ScrollPane();
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        StackPane stackPane = new StackPane();
        stackPane.setAlignment(Pos.CENTER);
        VBox vBox2 = new VBox(30);
        Text t = new Text(); t.setText("\n\n");
        vBox2.getChildren().add(t);
        for (Tablet tablet : tabletList) {
            String desc=("\n\n\t\t\t-----------------------------------------------------------------------------------------------");
            desc+= "\t\t\t\t\t\t Producent:    " + tablet.company+"\n";
            desc+= ("\n\t\t\t\t\t\t Model:    " + tablet.name+"\n");
            desc+= ("\n\t\t\t\t\t\t System operacyjny:    " + tablet.OS+"\n");
            desc+= ("\n\t\t\t\t\t\t Wersja systemu:    " + tablet.OS_version+"\n");
            desc+= ("\n\t\t\t\t\t\t Cena:    " + tablet.price + " zł\n");
            desc+= ("\n\t\t\t\t\t\t Przekątna:    " + tablet.display_size + " \""+"\n");
            desc+= ("\n\t\t\t\t\t\t Pamięć wewnętrzna:    " + tablet.memory + " GB\n");
            desc+= ("\n\t\t\t\t\t\t Pamięć operacyjna:    " + tablet.RAM + " GB\n");
            desc+= ("\n\t\t\t\t\t\t Dodatokowe:    " + tablet.additional);
            Text text = new Text(desc);
            text.setFont(Font.font("Verdana", FontWeight.MEDIUM, 15));
            vBox2.getChildren().add(text);
        }
        t = new Text(); t.setText("\n\n");
        vBox2.getChildren().add(t);
        sp.setContent(vBox2);
        vBox.getChildren().add(sp);
    }

    // Smartphone Result //
    public void set_smartphone_result(List<Smartphone> smartphoneList) {
        stackPane_question.getChildren().clear();
        vBox.getChildren().clear();
        // History //
        Text history;
        history = new Text("Historia:   \n");
        StackPane stackPane_history = new StackPane();
        history.setFont(Font.font("Verdana", FontWeight.BOLD, 20));;
        stackPane_history.getChildren().add(history);
        stackPane_history.setPadding(new Insets(20, 20, 20, 20));
        stackPane_history.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        vBox.getChildren().add(stackPane_history);
        for(String h : Decisions.history) {
            if (Decisions.history.indexOf(h) == Decisions.history.size() - 1) {
                history = new Text(h);
            } else {
                history = new Text(h + "   --->  ");
            }
            history.setFont(Font.font("Verdana", FontWeight.MEDIUM, 15));;
            hBox.getChildren().add(history);
        }
        hBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(hBox);
        // End //
        Text question_text = new Text("Wynik:");
        question_text.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        stackPane_question.getChildren().add(question_text);
        stackPane_question.setPadding(new Insets(20, 20, 20, 20));
        stackPane_question.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        vBox.getChildren().add(stackPane_question);

        // Results //
        ScrollPane sp = new ScrollPane();
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        StackPane stackPane = new StackPane();
        stackPane.setAlignment(Pos.CENTER);
        VBox vBox2 = new VBox(30);
        Text t = new Text();
        vBox2.getChildren().add(t);
        String dualSim, SdCard;
        for (Smartphone smartphone : smartphoneList) {
            if (smartphone.SD_availble==Boolean.FALSE){SdCard="Brak";}
            else {SdCard="Tak";}
            if (smartphone.dual_sim==Boolean.FALSE){dualSim="Brak";}
            else {dualSim="Tak";}
            String desc=("\n\n\t\t\t-----------------------------------------------------------------------------------------------"+"\n");
            desc+= "\n\t\t\t\t\t\t Producent:  "+smartphone.company+"\n";
            desc+= ("\n\t\t\t\t\t\t Model:  "+smartphone.name+"\n");
            desc+= ("\n\t\t\t\t\t\t System operacyjny:  "+smartphone.OS+"\n");
            desc+= ("\n\t\t\t\t\t\t Wersja systemu:  "+smartphone.OS_version+"\n");
            desc+= ("\n\t\t\t\t\t\t Cena:  "+smartphone.price+" zł\n");
            desc+= ("\n\t\t\t\t\t\t Przekątna:  "+smartphone.display_size + " \""+"\n");
            desc+= ("\n\t\t\t\t\t\t Pamięć wbudowana:  "+smartphone.memory + " GB\n");
            desc+= ("\n\t\t\t\t\t\t Pamięć operacyjna:  "+smartphone.RAM + " GB\n");
            desc+= ("\n\t\t\t\t\t\t Miejsce na kartę SD:  "+SdCard+"\n");
            desc+= ("\n\t\t\t\t\t\t Aparat tylni:  "+smartphone.back_camera + " Mpx\n");
            desc+= ("\n\t\t\t\t\t\t Aparat przedni:  "+smartphone.front_camera + " Mpx\n");
            desc+= ("\n\t\t\t\t\t\t Dual Sim:  "+dualSim+"\n");
//            desc+= ("\n\t\t\t\t\t\t Type:  "+smartphone.type);
            Text text = new Text(desc);
            text.setFont(Font.font("Verdana", FontWeight.MEDIUM, 15));
            vBox2.getChildren().add(text);
        }
        t = new Text(); t.setText("\n\n");
        vBox2.getChildren().add(t);
        sp.setContent(vBox2);
        vBox.getChildren().add(sp);

    }

    // Laptop Result //
    public void set_laptop_result(List<Laptop> laptopList){
        stackPane_question.getChildren().clear();
        vBox.getChildren().clear();
        // History //
        Text history;
        history = new Text("Historia:   \n");
        StackPane stackPane_history = new StackPane();
        history.setFont(Font.font("Verdana", FontWeight.BOLD, 20));;
        stackPane_history.getChildren().add(history);
        stackPane_history.setPadding(new Insets(20, 20, 20, 20));
        stackPane_history.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        vBox.getChildren().add(stackPane_history);
        for(String h : Decisions.history) {
            if (Decisions.history.indexOf(h) == Decisions.history.size() - 1) {
                history = new Text(h);
            } else {
                history = new Text(h + "   --->  ");
            }
            history.setFont(Font.font("Verdana", FontWeight.MEDIUM, 15));;
            hBox.getChildren().add(history);
        }
        hBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(hBox);

        // End //
        Text question_text = new Text("Wynik:");
        question_text.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        stackPane_question.getChildren().add(question_text);
        stackPane_question.setPadding(new Insets(20, 20, 20, 20));
        stackPane_question.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        vBox.getChildren().add(stackPane_question);

        // Results //
        ScrollPane sp = new ScrollPane();
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        StackPane stackPane = new StackPane();
        stackPane.setAlignment(Pos.CENTER);
        VBox vBox2 = new VBox(30);
        Text t = new Text();
        vBox2.getChildren().add(t);
        for (Laptop laptop : laptopList) {
            String desc=("\n\n\t\t\t-----------------------------------------------------------------------------------------------");
            desc+= "\t\t\t\t\t\t Producent:    "+laptop.company+"\n";
            desc+= ("\n\t\t\t\t\t\t Model:  "+laptop.name+"\n");
            desc+= ("\n\t\t\t\t\t\t System operacyjny:  "+laptop.OS+"\n");
            desc+= ("\n\t\t\t\t\t\t Wersja systemu:  "+laptop.OS_version+"\n");
            desc+= ("\n\t\t\t\t\t\t Cena:   "+laptop.price + " zł\n");
            desc+= ("\n\t\t\t\t\t\t Przekątna:  "+laptop.display_size+" \"\n");
            desc+= ("\n\t\t\t\t\t\t Dysk SSD:   "+laptop.disk + " GB\n");
            desc+= ("\n\t\t\t\t\t\t Pamięć RAM: "+laptop.RAM + " GB\n");
            desc+= ("\n\t\t\t\t\t\t Waga:   "+laptop.weight + " kg\n");
            desc+= ("\n\t\t\t\t\t\t Karta graficzna:    "+laptop.graphic_card+"\n");
            desc+= ("\n\t\t\t\t\t\t Producent Karty graficznej: "+laptop.graphic_card_prod+"\n");
            desc+= ("\n\t\t\t\t\t\t Procesor:   "+laptop.processor+"\n");
//            desc+= ("\n\t\t\t\t\t\t "+laptop.type+"\n");
            desc+= ("\n\t\t\t\t\t\t Dodatkowe Cechy:    "+laptop.additional);
            Text text = new Text(desc);
            text.setFont(Font.font("Verdana", FontWeight.MEDIUM, 15));
            vBox2.getChildren().add(text);
        }
        t = new Text();
        t.setText("\n\n");
        vBox2.getChildren().add(t);
        sp.setContent(vBox2);
        vBox.getChildren().add(sp);
    }
}
