package sample;

import java.util.List;

public class Question {

    public String question;
    public List<String> answers;

    Question(String question_, List<String> answers_){
        this.question = question_;
        this.answers = answers_;
    }
}
