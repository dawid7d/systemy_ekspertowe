package sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class QuestionList {

    public HashMap<Integer,Question> question_index = new HashMap();
    public HashMap<Integer,Question> tablet_index = new HashMap();
    public HashMap<Integer,Question> smartphone_index = new HashMap();
    public HashMap<Integer,Question> laptop_index = new HashMap();
    public String question;

    QuestionList(){
        // Device //
        question = "Wybierz jakiego rodzaju ma to być urządzenie mobilne:";
        List<String> temp = new ArrayList<>();
        temp.add("Laptop");
        temp.add("Tablet");
        temp.add("Smartphone");
        question_index.put(0,new Question(question,temp));

        // Result Question //
        question = "Wynik systemu:";
        temp = new ArrayList<>();
        question_index.put(999,new Question(question,temp));

        laptop_questions();
        tablet_questions();
        smartphone_questions();
    }

    private void laptop_questions(){
        question = "Wybierz główne zastosowanie urządzenia:";
        List<String> temp = new ArrayList<>();
        temp.add("Gra");
        temp.add("Praca");
        temp.add("Przeglądanie internetu");
        laptop_index.put(0,new Question(question,temp));

        // Game -> Price //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("3000 - 5000 zł");
        temp.add("5000 - 7000 zł");
        temp.add("7000 - 10000 zł");
        temp.add("10000 - 20000 zł");
        laptop_index.put(1,new Question(question,temp));

        // Game -> Graphic Card //
        question = "Wybierz jaka ma być karta graficzna:";
        temp = new ArrayList<>();
        temp.add("Nividia");
        temp.add("AMD");
        laptop_index.put(2,new Question(question,temp));

        // Game -> RAM //
        question = "Jak duża ma być pamięć operacyjna?";
        temp = new ArrayList<>();
        temp.add("8 GB");
        temp.add("16 GB");
        laptop_index.put(3,new Question(question,temp));

        question = "Wybierz jaki ma być system operacyjny:";
        temp = new ArrayList<>();
        temp.add("iOS");
        temp.add("Windows");
        temp.add("Bez systemu op.");
        laptop_index.put(4,new Question(question,temp));

        // Work -> McBook -> Price //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("4000 - 7000 zł");
        temp.add("7000 - 10000 zł");
        temp.add("10000 - 15000 zł");
        temp.add("15000 - 30000 zł");
        laptop_index.put(5,new Question(question,temp));

        // Work -> McBook -> RAM_1 //
        question = "Wybierz jak duża ma być pamięć operacyjna:";
        temp = new ArrayList<>();
        temp.add("8 GB");
        temp.add("16 GB");
        laptop_index.put(6,new Question(question,temp));

        // Work -> McBook -> RAM_2 //
        question = "Wybierz jak duża ma być pamięć operacyjna:";
        temp = new ArrayList<>();
        temp.add("16 GB");
        temp.add("32 GB");
        laptop_index.put(7,new Question(question,temp));

        // Work -> Windows, Bez_syst -> Price //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("2000 - 4000 zł");
        temp.add("4000 - 60000 zł");
        temp.add("6000 - 8000 zł");
        temp.add("8000 - 10000 zł");
        temp.add("10000 - 20000 zł");
        laptop_index.put(8,new Question(question,temp));

        // Work -> Windows -> RAM_1 //
        question = "Wybierz jak duża ma być pamięć operacyjna:";
        temp = new ArrayList<>();
        temp.add("4 GB");
        temp.add("8 GB");
        temp.add("16 GB");
        laptop_index.put(9,new Question(question,temp));

        // Work -> Windows,iOS -> RAM_2 //
        question = "Wybierz jak duża ma być pamięć operacyjna:";
        temp = new ArrayList<>();
        temp.add("8 GB");
        temp.add("16 GB");
        temp.add("32 GB");
        laptop_index.put(10,new Question(question,temp));

        // Internet //
        question = "Jakia ma być główne zastosowanie laptopa?";
        temp = new ArrayList<>();
        temp.add("Wytrzymała Bateria");
        temp.add("Funkcja Tabletu (2w1)");
        laptop_index.put(11,new Question(question,temp));

        // Internet -> Battery -> Price //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("4000 - 6000 zł");
        temp.add("6000 - 8000 zł");
        temp.add("8000 - 10000 zł");
        laptop_index.put(12,new Question(question,temp));

        // Internet -> 2in1 -> Price //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("0 - 2000 zł");
        temp.add("2000 - 4000 zł");
        temp.add("4000 - 6000 zł");
        temp.add("7000 - 10000 zł");
        laptop_index.put(13,new Question(question,temp));

        // Game -> RAM_2 //
        question = "Wybierz jak duża ma być pamięć operacyjna:";
        temp = new ArrayList<>();
        temp.add("16 GB");
        temp.add("32 GB");
        laptop_index.put(14,new Question(question,temp));

        // Work -> RAM_3 //
        question = "Wybierz jak duża ma być pamięć operacyjna:";
        temp = new ArrayList<>();
        temp.add("32 GB");
        laptop_index.put(15,new Question(question,temp));

        // Work -> No OS //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("2000 - 4000 zł");
        temp.add("4000 - 6000 zł");
        temp.add("6000 - 8000 zł");
        temp.add("8000 - 10000 zł");
        laptop_index.put(16,new Question(question,temp));
    }

    private void smartphone_questions(){
        // Type //
        question = "Jakie ma być główne zastosowanie SmartPhona?";
        List<String> temp = new ArrayList<>();
        temp.add("Uniwersalny");
        temp.add("Gra");
        temp.add("Najlepszy Aparat");
        temp.add("Wytrzymałość obudowy");
        smartphone_index.put(0,new Question(question,temp));

        // Game //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("2000-4000 zł");
        temp.add("4000-7500 zł");
        smartphone_index.put(1,new Question(question,temp));

        // Resistant //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("0-1500 zł");
        temp.add("1500-3500 zł");
        smartphone_index.put(2,new Question(question,temp));

        // Universal --> OS //
        question = "Wybierz jaki ma być system operacyjny:";
        temp = new ArrayList<>();
        temp.add("Android");
        temp.add("iOS");
        temp.add("Windows");
        temp.add("BlackBerry");
        smartphone_index.put(3,new Question(question,temp));

        // Universal --> iOS //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("1000-2500 zł");
        temp.add("2500-4000 zł");
        temp.add("4000-5500 zł");
        temp.add("5500-7500 zł");
        smartphone_index.put(4,new Question(question,temp));

        // iOS  --> pamiec 1-2,5k//
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("32 GB");
        temp.add("128 GB");
        smartphone_index.put(5,new Question(question,temp));

        // iOS  --> pamiec 2,5-4k//
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("64 GB");
        temp.add("128 GB");
        temp.add("256 GB");
        smartphone_index.put(6,new Question(question,temp));

        // iOS  --> pamiec 4-5,5k//
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("64 GB");
        temp.add("256 GB");
        smartphone_index.put(7,new Question(question,temp));

        // iOS  --> pamiec 5,5-7,5k//
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("256 GB");
        temp.add("512 GB");
        smartphone_index.put(8,new Question(question,temp));

        // Universal --> Android //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("0-500 zł");
        temp.add("500-1500 zł");
        temp.add("1500-2500 zł");
        temp.add("2500-4000 zł");
        temp.add("4000-5500 zł");
        smartphone_index.put(9,new Question(question,temp));

        // Universal --> Android //
        question = "Czy potrzebny jest Dual Sim?";
        temp = new ArrayList<>();
        temp.add("Tak");
        temp.add("Nie");
        smartphone_index.put(10,new Question(question,temp));

        // Universal --> Android //
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("8 GB");
        temp.add("16 GB");
        temp.add("32 GB");
        smartphone_index.put(11,new Question(question,temp));

        // Universal --> Android //
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("16 GB");
        temp.add("32 GB");
        temp.add("64 GB");
        smartphone_index.put(12,new Question(question,temp));

        // Universal --> Android //
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("64 GB");
        temp.add("128 GB");
        temp.add("256 GB");
        smartphone_index.put(13,new Question(question,temp));

        // Universal --> Android //
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("128 GB");
        temp.add("256 GB");
        temp.add("512 GB");
        smartphone_index.put(14,new Question(question,temp));

        // Universal --> Android //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("2000-3500 zł");
        temp.add("3500-6000 zł");
        smartphone_index.put(15,new Question(question,temp));
    }

    private void tablet_questions(){
        // OS ? //
        question = "Wybierz jaki ma być system operacyjny:";
        List<String> temp = new ArrayList<>();
        temp.add("Android");
        temp.add("iOS");
        temp.add("Windows");
        tablet_index.put(0,new Question(question,temp));

        // Android //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("200-800 zł");
        temp.add("800-1500 zł");
        temp.add("1500-3500 zł");
        tablet_index.put(1,new Question(question,temp));

        // iOS //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("1500-3000 zł");
        temp.add("3000-6200 zł");
        tablet_index.put(2,new Question(question,temp));

        // Windows //
        question = "Wybierz z jakiego przedziału cenowego ma być urządzienie:";
        temp = new ArrayList<>();
        temp.add("800-1500 zł");
        temp.add("1500-3000 zł");
        tablet_index.put(3,new Question(question,temp));

        // iOS -> 1.5-3K,  Przekatna? //
        question = "Wybierz jaka duża ma być przekątna ekranu:";
        temp = new ArrayList<>();
        temp.add("7.9");
        temp.add("9.7");
        temp.add("10.5");
        tablet_index.put(4,new Question(question,temp));

        // iOS -> 3-6K,  Przekatna? //
        question = "Wybierz jaka duża ma być przekątna ekranu:";
        temp = new ArrayList<>();
        temp.add("10.5");
        temp.add("12.9");
        tablet_index.put(5,new Question(question,temp));

        // iOS -> 1.5-3K, 9.7",  Pamiec? //
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("32 GB");
        temp.add("64 GB");
        temp.add("128 GB");
        tablet_index.put(6,new Question(question,temp));

        // iOS -> 1.5-3K, 10.5 && 12.5",  Pamiec? //
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("64 GB");
        temp.add("128 GB");
        temp.add("512 GB");
        tablet_index.put(7,new Question(question,temp));

        // Android -> 0.2-0.8k, przekatna? //
        question = "Wybierz jaka duża ma być przekątna ekranu:";
        temp = new ArrayList<>();
        temp.add("7");
        temp.add("8");
        temp.add("10.1");
        tablet_index.put(8,new Question(question,temp));

        // Android -> 0.8-1.5k && 1.5-3.5k, przekatna? //
        question = "Wybierz jaka duża ma być przekątna ekranu:";
        temp = new ArrayList<>();
        temp.add("8");
        temp.add("10");
        tablet_index.put(9,new Question(question,temp));

        // Android -> 0.8-1.5k , 10" pamiec? //
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("16 GB");
        temp.add("32 GB");
        tablet_index.put(10,new Question(question,temp));

        // Android -> 1.5-3.5k, 10" pamiec? //
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("32 GB");
        temp.add("64 GB");
        tablet_index.put(11,new Question(question,temp));

        // Windows -> 0.8-1.5k, przekatna? //
        question = "Wybierz jaka duża ma być przekątna ekranu:";
        temp = new ArrayList<>();
        temp.add("10.1");
        temp.add("11.6");
        tablet_index.put(12,new Question(question,temp));

        // Windows -> 1.5-3.5k, przekatna? //
        question = "Wybierz jaka duża ma być przekątna ekranu:";
        temp = new ArrayList<>();
        temp.add("8");
        temp.add("10");
        temp.add("12");
        tablet_index.put(13,new Question(question,temp));

        // iOS -> 3-6K, 10.5 && 12.5",  Pamiec? //
        question = "Wybierz jak duża ma być pamięć wbudowana:";
        temp = new ArrayList<>();
        temp.add("64 GB");
        temp.add("256 GB");
        temp.add("512 GB");
        tablet_index.put(14,new Question(question,temp));
    }
}
