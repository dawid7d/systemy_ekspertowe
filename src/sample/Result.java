package sample;


import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Result {

    public static List<Tablet> result_tablet_list = new ArrayList<>();
    public static List<Smartphone> result_smartphone_list = new ArrayList<>();
    public static List<Laptop> result_laptop_list = new ArrayList<>();

    // Tablet --------------------------------------------------------------------------------------------------------//
    public static List<Tablet> make_tablet_result(List<String> history) {
        TabletList tabletList = new TabletList();
            result_tablet_list = tabletList.tablet_list;
            Iterator<Tablet> it = result_tablet_list.iterator();

            while (it.hasNext()) {
                Tablet tab = it.next();
                // OS //
                if (!tab.OS.equals(history.get(1))) {
                    it.remove(); continue;
                }
                // Price //
                String[] tmp = history.get(2).split("-");
                tmp[1] = tmp[1].substring(0, tmp[1].length() - 2);
                if (tab.price < Double.parseDouble(tmp[0]) || tab.price > Double.parseDouble(tmp[1])) {
                    it.remove(); continue;
                }
                // Display Size //
                if (tab.display_size < Double.parseDouble(history.get(3)) || tab.display_size > Double.parseDouble(history.get(3))) {
                    it.remove(); continue;
                }
                // Memory (if has) //
                if (history.size() >= 5) {
                    Integer memory = Integer.parseInt(history.get(4).split(" ")[0]);
                    if (tab.memory < memory || tab.memory > memory) {
                        it.remove(); continue;
                    }
                }
            }
        return result_tablet_list;
    }

    // Smartphone ----------------------------------------------------------------------------------------------------//
    public static List<Smartphone> make_smartphone_result(List<String> history) {
            SmartphoneList smartphoneList = new SmartphoneList();
            result_smartphone_list = smartphoneList.smartphone_list;
            Iterator<Smartphone> it = result_smartphone_list.iterator();
            Boolean match;

            while (it.hasNext()) {
                Smartphone smtp = it.next();
                String[] types = smtp.type.split(",");
                // Type //
                if (history.get(1)=="Wytrzymałość obudowy"){
                    match = Arrays.stream(types).noneMatch("Work"::equals);
                    if (match) {
                        it.remove(); continue;
                    }
                    String[] tmp = history.get(2).split("-");
                    tmp[1] = tmp[1].substring(0, tmp[1].length() - 2);
                    if (smtp.price < Double.parseDouble(tmp[0]) || smtp.price > Double.parseDouble(tmp[1])) {
                        it.remove(); continue;
                    }
                }
                else if(history.get(1)=="Najlepszy Aparat"){
                    // Price //
                    String[] tmp = history.get(2).split("-");
                    tmp[1] = tmp[1].substring(0, tmp[1].length() - 2);
                    if (smtp.price < Double.parseDouble(tmp[0]) || smtp.price > Double.parseDouble(tmp[1])) {
                        it.remove(); continue;
                    }
                    // Type //
                    types = smtp.type.split(",");
                    match = Arrays.stream(types).noneMatch("Camera"::equals);
                    if (match) {
                        it.remove(); continue;
                    }
                }
                else if(history.get(1)=="Gra"){
                    types = smtp.type.split(",");
                    match = Arrays.stream(types).noneMatch("Game"::equals);
                    if (match) {
                        it.remove(); continue;
                    }
                    String[] tmp = history.get(2).split("-");
                    tmp[1] = tmp[1].substring(0, tmp[1].length() - 2);
                    if (smtp.price < Double.parseDouble(tmp[0]) || smtp.price > Double.parseDouble(tmp[1])) {
                        it.remove(); continue;
                    }
                }
                else if (history.get(1) == "Uniwersalny") {
                    if (!history.get(2).equals(smtp.OS)) {
                        it.remove(); continue;
                    }
                    // Price //
                    if(history.size()>3){
                        String[] tmp = history.get(3).split("-");
                        tmp[1] = tmp[1].substring(0, tmp[1].length() - 2);
                        if (smtp.price < Double.parseDouble(tmp[0]) || smtp.price > Double.parseDouble(tmp[1])) {
                            it.remove(); continue;
                        }
                    }
                    // Android //
                    if(history.get(2)=="Android"){
                        if(history.get(4)=="Nie" && smtp.dual_sim==Boolean.TRUE){
                            it.remove(); continue;
                        }
                        if(history.get(4)=="Tak" && smtp.dual_sim==Boolean.FALSE){
                            it.remove(); continue;
                        }
                        if(history.get(3)=="4000-5500 zł"){
                            String[] tmp = history.get(4).split(" ");
                            if(smtp.memory!=Integer.parseInt(tmp[0])){
                                it.remove(); continue;
                            }
                        } else {
                            String[] tmp = history.get(5).split(" ");
                            if(smtp.memory!=Integer.parseInt(tmp[0])){
                                it.remove(); continue;
                            }
                        }
                    }
                    // iOS //
                    if(history.get(2)=="iOS"){
                        String[] tmp = history.get(4).split(" ");
                        if(smtp.memory!=Integer.parseInt(tmp[0])){
                            it.remove(); continue;
                        }
                    }
                }
            }
        return  smartphoneList.smartphone_list;
    }

    // Laptop --------------------------------------------------------------------------------------------------------//
    public static List<Laptop> make_laptop_result(List<String> history) {
        LaptopList laptopList = new LaptopList();
        result_laptop_list = laptopList.laptop_list;
        Iterator<Laptop> it = result_laptop_list.iterator();
        Boolean match;

        while (it.hasNext()) {
            Laptop lap = it.next();

            // Internet //
            if(history.get(2)=="Funkcja Tabletu (2w1)"){
                // Type //
                if(lap.type!="2in1"){
                    it.remove();
                    continue;
                }
                // Price //
                if(history.size()>3){
                    String[] tmp = history.get(3).split("-");
                    tmp[1] = tmp[1].substring(0, tmp[1].length() - 2);
                    if (lap.price < Double.parseDouble(tmp[0]) || lap.price > Double.parseDouble(tmp[1])) {
                        it.remove();
                        continue;
                    }
                }
            }
            if(history.get(2)=="Wytrzymała Bateria"){
                // Type //
                if(lap.type!="Battery"){
                    it.remove();
                    continue;
                }
                // Price //
                String[] tmp = history.get(3).split("-");
                tmp[1] = tmp[1].substring(0, tmp[1].length() - 2);
                if (lap.price < Double.parseDouble(tmp[0]) || lap.price > Double.parseDouble(tmp[1])) {
                    it.remove();
                    continue;
                }
            }
            // Work //
            if(history.get(1)=="Praca"){
                // Type //
                String[] types = lap.type.split(",");
                match = Arrays.stream(types).noneMatch("Work"::equals);
                if (match) {
                    it.remove();
                    continue;
                }
                // Price //
                String[] tmp = history.get(3).split("-");
                tmp[1] = tmp[1].substring(0, tmp[1].length() - 2);
                if (lap.price < Double.parseDouble(tmp[0]) || lap.price > Double.parseDouble(tmp[1])) {
                    it.remove();
                    continue;
                }
                // OS //
                if (lap.OS!=history.get(2)) {
                    it.remove();
                    continue;
                }
                // RAM //
                if(history.size()>4){
                    tmp = history.get(4).split(" ");
                    if (lap.RAM!=Integer.parseInt(tmp[0])){
                        it.remove();
                        continue;
                    }
                }
            }
            // Work //
            if(history.get(1)=="Uniwersalny"){
                // Type //
                if(lap.type!="Universal"){
                    it.remove();
                    continue;
                }
                // Price //
                String[] tmp = history.get(3).split("-");
                tmp[1] = tmp[1].substring(0, tmp[1].length() - 2);
                if (lap.price < Double.parseDouble(tmp[0]) || lap.price > Double.parseDouble(tmp[1])) {
                    it.remove();
                    continue;
                }
                // OS //
                if (lap.OS!=history.get(2)) {
                    it.remove();
                    continue;
                }
                // RAM //
                tmp = history.get(4).split(" ");
                if (lap.RAM!=Integer.parseInt(tmp[0])){
                    it.remove();
                    continue;
                }
            }
            // Game //
            if(history.get(1)=="Gra"){
                // Type //
                String[] types = lap.type.split(",");
                match = Arrays.stream(types).noneMatch("Game"::equals);
                if (match) {
                    it.remove();
                    continue;
                }
                // Price //
                String[] tmp = history.get(2).split("-");
                tmp[1] = tmp[1].substring(0, tmp[1].length() - 2);
                if (lap.price < Double.parseDouble(tmp[0]) || lap.price > Double.parseDouble(tmp[1])) {
                    it.remove();
                    continue;
                }
                // Grap.Card.Prod //
                if (!lap.graphic_card_prod.equals(history.get(3))){
                    it.remove();
                    continue;
                }
                // RAM //
                tmp = history.get(4).split(" ");
                if (lap.RAM!=Integer.parseInt(tmp[0])){
                    it.remove();
                    continue;
                }
            }

        }
        return result_laptop_list;
    }

}


