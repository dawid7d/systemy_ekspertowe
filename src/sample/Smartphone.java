package sample;

public class Smartphone {

    public String name;
    public Double price;
    public String company;
    public String OS;
    public String OS_version;
    public Double display_size;
    public Integer memory;
    public Integer RAM;
    public Boolean dual_sim;
    public Integer front_camera;
    public Integer back_camera;
    public Integer battery;
    public Boolean SD_availble;
    public String type;
    public String procesor;
    public String additional;
}
