package sample;

public class Tablet {

    public String name;
    public Double price;
    public String company;
    public String OS;
    public String OS_version;
    public Double display_size;
    public Integer memory;
    public Integer RAM;
    public String additional = "";
}