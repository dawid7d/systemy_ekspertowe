package sample;

import java.util.ArrayList;
import java.util.List;

public class TabletList {

    List<Tablet> tablet_list = new ArrayList<>();
    Tablet new_tablet;

    TabletList(){
        // iOS //
        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Gold";
        new_tablet.price = 1550.00;
        new_tablet.memory = 16;
        new_tablet.RAM = 2;
        new_tablet.display_size = 10.5;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "11";
        new_tablet.company = "Apple";
        new_tablet.additional = "Rodzielczość UHD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Gold";
        new_tablet.price = 1599.00;
        new_tablet.memory = 32;
        new_tablet.RAM = 2;
        new_tablet.display_size = 9.7;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "11";
        new_tablet.company = "Apple";
        new_tablet.additional = "Rodzielczość UHD, Kolor Srebny";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Gold";
        new_tablet.price = 1599.00;
        new_tablet.memory = 64;
        new_tablet.RAM = 2;
        new_tablet.display_size = 9.7;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "11";
        new_tablet.company = "Apple";
        new_tablet.additional = "Głośniki stereo, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Black";
        new_tablet.price = 1999.00;
        new_tablet.memory = 128;
        new_tablet.RAM = 2;
        new_tablet.display_size = 9.7;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "11";
        new_tablet.company = "Apple";
        new_tablet.additional = "Rodzielczość UHD,Blokowanie na odcisk palca";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Pro";
        new_tablet.price = 3699.00;
        new_tablet.memory = 64;
        new_tablet.RAM = 2;
        new_tablet.display_size = 10.5;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "10";
        new_tablet.company = "Apple";
        new_tablet.additional = "Głośniki stereo, Blokowanie na odcisk palca";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Pro";
        new_tablet.price = 4350.00;
        new_tablet.memory = 256;
        new_tablet.RAM = 2;
        new_tablet.display_size = 10.5;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "10";
        new_tablet.company = "Apple";
        new_tablet.additional = "Rodzielczość UHD, Kolor Czarny";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Pro";
        new_tablet.price = 5300.00;
        new_tablet.memory = 512;
        new_tablet.RAM = 2;
        new_tablet.display_size = 10.5;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "10";
        new_tablet.company = "Apple";
        new_tablet.additional = "Rodzielczość UHD, Blokowanie na odcisk palca";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Pro";
        new_tablet.price = 3749.00;
        new_tablet.memory = 64;
        new_tablet.RAM = 2;
        new_tablet.display_size = 12.9;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "10";
        new_tablet.company = "Apple";
        new_tablet.additional = "Rodzielczość UHD, Blokowanie na odcisk palca";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Pro";
        new_tablet.price = 3749.00;
        new_tablet.memory = 64;
        new_tablet.RAM = 2;
        new_tablet.display_size = 12.9;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "10";
        new_tablet.company = "Apple";
        new_tablet.additional = "Rodzielczość UHD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Pro";
        new_tablet.price = 4499.00;
        new_tablet.memory = 256;
        new_tablet.RAM = 2;
        new_tablet.display_size = 12.9;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "10";
        new_tablet.company = "Apple";
        new_tablet.additional = "Rodzielczość UHD, Kolor Czarny";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad Pro";
        new_tablet.price = 5449.00;
        new_tablet.memory = 512;
        new_tablet.RAM = 2;
        new_tablet.display_size = 12.9;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "10";
        new_tablet.company = "Apple";
        new_tablet.additional = "Pamięć 512GB, Kolor Srebny";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad mini";
        new_tablet.price = 2699.00;
        new_tablet.memory = 128;
        new_tablet.RAM = 2;
        new_tablet.display_size = 7.9;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "9";
        new_tablet.company = "Apple";
        new_tablet.additional = "Rodzielczość 2048x1536, Kolor Czarny";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Apple iPad mini";
        new_tablet.price = 1870.00;
        new_tablet.memory = 64;
        new_tablet.RAM = 2;
        new_tablet.display_size = 7.9;
        new_tablet.OS = "iOS";
        new_tablet.OS_version = "9";
        new_tablet.company = "Apple";
        new_tablet.additional = "Blokowanie na odcisk palca, Kolor Biały";
        tablet_list.add(new_tablet);

        // Windows //
        new_tablet = new Tablet();
        new_tablet.name = "Kruger&Matz 2w1 EDGE 1162";
        new_tablet.price = 999.00;
        new_tablet.memory = 32;
        new_tablet.RAM = 4;
        new_tablet.display_size = 11.6;
        new_tablet.OS = "Windows";
        new_tablet.OS_version = "10 PL";
        new_tablet.company = "Kruger&Matz";
        new_tablet.additional = "Slot SD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Lenovo Miix 320-10";
        new_tablet.price = 999.00;
        new_tablet.memory = 64;
        new_tablet.RAM = 2;
        new_tablet.display_size = 10.1;
        new_tablet.OS = "Windows";
        new_tablet.OS_version = "10 PL";
        new_tablet.company = "Levono";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Kruger&Matz EDGE 1086";
        new_tablet.price = 849.00;
        new_tablet.memory = 32;
        new_tablet.RAM = 2;
        new_tablet.display_size = 10.1;
        new_tablet.OS = "Windows";
        new_tablet.OS_version = "10 PL";
        new_tablet.company = "Kruger&Matz";
        new_tablet.additional = "Slot SD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "YOGA Book";
        new_tablet.price = 2649.00;
        new_tablet.memory = 128;
        new_tablet.RAM = 4;
        new_tablet.display_size = 10.1;
        new_tablet.OS = "Windows";
        new_tablet.OS_version = "10 PL";
        new_tablet.company = "Lenovo";
        tablet_list.add(new_tablet);
        new_tablet = new Tablet();

        new_tablet.name = "Caterpillar T20";
        new_tablet.price = 1949.00;
        new_tablet.memory = 64;
        new_tablet.RAM = 2;
        new_tablet.display_size = 8.0;
        new_tablet.OS = "Windows";
        new_tablet.OS_version = "10 PL";
        new_tablet.company = "Cat";
        new_tablet.additional = "Slot SD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Surace GO";
        new_tablet.price = 1999.00;
        new_tablet.memory = 64;
        new_tablet.RAM = 4;
        new_tablet.display_size = 10.0;
        new_tablet.OS = "Windows";
        new_tablet.OS_version = "10 PL";
        new_tablet.company = "Microsoft";
        new_tablet.additional = "Slot SD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Surace GO";
        new_tablet.price = 2899.00;
        new_tablet.memory = 128;
        new_tablet.RAM = 8;
        new_tablet.display_size = 10.0;
        new_tablet.OS = "Windows";
        new_tablet.OS_version = "10 PL";
        new_tablet.company = "Microsoft";
        tablet_list.add(new_tablet);
        new_tablet.additional = "Slot SD, Kolor Biały";
        new_tablet = new Tablet();

        new_tablet.name = "Galxy Book 12";
        new_tablet.price = 2999.00;
        new_tablet.memory = 64;
        new_tablet.RAM = 8;
        new_tablet.display_size = 12.0;
        new_tablet.OS = "Windows";
        new_tablet.OS_version = "10 PL";
        new_tablet.company = "Samsung";
        new_tablet.additional = "Slot SD, Kolor Biały";
        tablet_list.add(new_tablet);


        // Android //
        new_tablet = new Tablet();
        new_tablet.name = "Lenovo YOGA Book";
        new_tablet.price = 2000.00;
        new_tablet.memory =  64;
        new_tablet.RAM = 4;
        new_tablet.display_size = 10.0;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "6.0 Marshmallow";
        new_tablet.company = "Lenovo";
        new_tablet.additional = "Rodzielczość Full HD";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Lenovo YOGA Book";
        new_tablet.price = 1699.00;
        new_tablet.memory =  32;
        new_tablet.RAM = 4;
        new_tablet.display_size = 10.0;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "6.0 Marshmallow";
        new_tablet.company = "Lenovo";
        new_tablet.additional = "Slot SD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Lenovo YOGA Book";
        new_tablet.price = 2300.00;
        new_tablet.memory =  32;
        new_tablet.RAM = 8;
        new_tablet.display_size = 10.0;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Lenovo";
        new_tablet.additional = "Slot SD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Galaxy Tab S2";
        new_tablet.price = 1700.00;
        new_tablet.memory =  32;
        new_tablet.RAM = 4;
        new_tablet.display_size = 8.0;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Samsung";
        new_tablet.additional = "Rodzielczość Full HD";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Galaxy Tab S2";
        new_tablet.price = 2500.00;
        new_tablet.memory =  64;
        new_tablet.RAM = 8;
        new_tablet.display_size = 8.0;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Samsung";
        new_tablet.additional = "Dostęony w 4 kolorach; Czarny,RóżowyBiały,Srebrny";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Galaxy Tab S2";
        new_tablet.price = 1099.00;
        new_tablet.memory =  32;
        new_tablet.RAM = 2;
        new_tablet.display_size = 8.0;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Samsung";
        new_tablet.additional = "Slot SD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Galaxy Tab S2";
        new_tablet.price = 1099.00;
        new_tablet.memory = 16;
        new_tablet.RAM = 4;
        new_tablet.display_size = 10.0;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Samsung";
        new_tablet.additional = "Głoścniki stereo";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Galaxy Tab S2";
        new_tablet.price = 1350.00;
        new_tablet.memory = 32;
        new_tablet.RAM = 4;
        new_tablet.display_size = 10.0;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Samsung";
        new_tablet.additional = "Slot SD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "MediaPad T3 10 ";
        new_tablet.price = 550.00;
        new_tablet.memory = 32;
        new_tablet.RAM = 4;
        new_tablet.display_size = 10.1;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Huawei";
        new_tablet.additional = "Pojemna bateria 3000mAh";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "TAB 4 10 APQ80";
        new_tablet.price = 520.00;
        new_tablet.memory = 16;
        new_tablet.RAM = 4;
        new_tablet.display_size = 10.1;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Lenovo";
        new_tablet.additional = "Slot SD, Kolor Biały";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "TAB 4 10 APQ80";
        new_tablet.price = 790.00;
        new_tablet.memory = 32;
        new_tablet.RAM = 4;
        new_tablet.display_size = 10.1;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Lenovo";
        new_tablet.additional = "Pojemna bateria 4000mAh";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Galaxy Tab A";
        new_tablet.price = 650.00;
        new_tablet.memory = 16;
        new_tablet.RAM = 4;
        new_tablet.display_size = 7.0;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Samsung";
        new_tablet.additional = "Czytnik linii papilarnych";
        tablet_list.add(new_tablet);

        new_tablet = new Tablet();
        new_tablet.name = "Galaxy Tab A";
        new_tablet.price = 750.00;
        new_tablet.memory = 16;
        new_tablet.RAM = 2;
        new_tablet.display_size = 8.0;
        new_tablet.OS = "Android";
        new_tablet.OS_version = "7.0";
        new_tablet.company = "Samsung";
        new_tablet.additional = "Slot SD, Kolor Czarny";
        tablet_list.add(new_tablet);
    }
}
